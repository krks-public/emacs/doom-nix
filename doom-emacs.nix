{ stdenv, nixpkgs }:
let
  callPackage = nixpkgs.callPackage;
  emacs = nixpkgs.emacs;
  

  emacsPkgOverrides = self: super: with self; {
      # inherit emacs;
      emacs-snippets = melpaBuild {
         pname = "emacs-snippets";
         ename = "emacs-snippets";
         version = "1.0.3";
         src = nixpkgs.fetchFromGitHub {
           owner = "hlissner";
           repo = "emacs-snippets";
           rev = "fbff5b10666dd555fedf4c651cfefd185d04e2f5";
           sha256 = "12x1vjjjxvjiy75x8z8w722kw5pv8pfin6p2mq95pw24s7rdryqh";
         };
         recipe = nixpkgs.writeText "recipe" ''
         (emacs-snippets :fetcher git :url "https://github.com/hlissner/emacs-snippets.git")
         '';
      };
      merlin = melpaBuild {
        pname = "merlin";
        ename = "merlin";
        version = "3.0";
        src = nixpkgs.fetchFromGitHub {
          owner = "ocaml";
          repo = "merlin";
          rev = "c408b1b91773445cd6521aa170766df99ffe081c";
          sha256 = "0v16nm960hz0s8b06yynmiq7msb11sncmnd5kzpf5h5g1sb8z6r3";
        };
        recipe = builtins.fetchurl {
          url = "https://raw.githubusercontent.com/milkypostman/melpa/13d1a86dfe682f65daf529f9f62dd494fd860be9/recipes/merlin";
          sha256 = "0r4wc5ann6239bagj364yyzw4y3lcpkl5nnn0vmx4hgkwdg509fn";
          name = "recipe";
        };
        packageRequires = [];
        meta = {
          homepage = "https://melpa.org/#/merlin";
          license = nixpkgs.lib.licenses.free;
        };
      };
      org-yt = melpaBuild {
        pname = "org-yt";
        ename = "org-yt";
        version = "20180527";
        src = nixpkgs.fetchFromGitHub {
           owner = "TobiasZawada";
           repo = "org-yt";
           rev = "40cc1ac76d741055cbefa13860d9f070a7ade001";
           sha256 = "0jsm3azb7lwikvc53z4p91av8qvda9s15wij153spkgjp83kld3p";
        };
        recipe = nixpkgs.writeText "recipe" ''
        (org-yt :fetcher github :repo "TobiasZawada/org-yt")
        '';
      };
      rotate-text = melpaBuild {
         pname = "rotate-text";
         ename = "rotate-text";
         version = "0.1";
         src = nixpkgs.fetchFromGitHub {
           owner = "debug-ito";
           repo = "rotate-text.el";
           rev = "48f193697db996855aee1ad2bc99b38c6646fe76";
           sha256 = "02fkv45nbbqrv12czlinpr8qpsd7yjdhr628hl58n42w560qxrs8";
         };
         recipe = nixpkgs.writeText "recipe" ''
         (rotate-text :fetcher github :repo "debug-ito/rotate-text.el")
         '';
      };
      evil-escape = melpaBuild {
        pname = "evil-escape";
        ename = "evil-escape";
        version = "3.15";
        src = nixpkgs.fetchFromGitHub {
            owner = "syl20bnr";
            repo = "evil-escape";
            rev = "73b30bfd912f40657b1306ee5849d215f0f9ffbd";
            sha256 = "0mqz7l1a4rxdj7g3fda17118f7y0vph4ica447ciad5vz3zx9i2z";
        };
        recipe = nixpkgs.writeText "recipe" ''
        (evil-escape :fetcher github :repo "syl20bnr/evil-escape")
        '';
      };
      # evil-escape = trivialBuild {
      #   pname = "evil-escape";
      #   ename = "evil-escape";
      #   version = "3.15";
      #   src = ./patched/evil-escape.el;
      #   phases = ["installPhase"];
      #   installPhase = ''
      #   LISPDIR=$out/share/emacs/site-lisp
      #   install -d $LISPDIR
      #   install $src $LISPDIR
      #   '';
      # };
      shrink-path = melpaBuild {
        pname = "shrink-path";
        ename = "shrink-path";
        version = "0.3.1";
        src = builtins.fetchGit {
          url = "https://gitlab.com/bennya/shrink-path.el.git";
          ref = "master";
          rev = "9d06c453d1537df46a4b703a29213cc7f7857aa0";
        };
        recipe = nixpkgs.writeText "recipe" ''
        (shrink-path :fetcher git :url "https://gitlab.com/bennya/shrink-path.el.git")
        '';
        packageRequires = [ dash f s ];
        meta = {
          homepage = "https://melpa.org/#/shrink-path";
          license = nixpkgs.lib.licenses.free;
        };
      };
  };
  
  emacsWithPackages =
    (nixpkgs.emacsPackagesNg.overrideScope' emacsPkgOverrides).emacsWithPackages;

  doomCorePackages = epkgs:
     with epkgs;
          [ async
            persistent-soft
            use-package
            quelpa
          ];
          
  
  doomEmacs = builtins.fetchGit {
      name = "doom-emacs";
      url = "https://github.com/hlissner/doom-emacs.git";
      ref = "develop";
      rev = "1610cd32b2c39070229fae44b8f925dce20f74e8";
    };

  emacsWithDoom = emacsPkgs:
     (emacsWithPackages emacsPkgs).overrideAttrs(attrs: {
          inherit doomEmacs;
          listPkgs = ./list-doom-packages.el;
          doomD = ./doom.d;
          postInstall = (attrs.postInstall or "") + ''
              mkdir -p $out/emacs.d
              cp -r $doomEmacs/* $out/emacs.d
              ln -s $doomD $out/doom.d

              # cannot just symlink $deps to .local because .local
              # needs to be writable and we don't have permissions to write to $deps
              # so we create .local and link everything from $deps to .local instead
              mkdir -p $out/emacs.d/.local/packages

              ln -s $deps/share/emacs/site-lisp/elpa  $out/emacs.d/.local/packages/elpa

              # ln -s $deps $out/emacs.d/.local
              ln -s $listPkgs $out/list-doom-packages.el

              echo "exporting package list"

              DOOM_EMACS_PKGS=$(
              DOOMDIR=$out/doom.d/ \
              EMACSD=$out/emacs.d/ \
              $out/bin/emacs \
                     --script $out/list-doom-packages.el \
                     
             )
             echo $DOOM_EMACS_PKGS > $out/doom-emacs-pkgs.nix
             echo $DOOM_EMACS_PKGS
             # EMACS=$out/bin/emacs $out/emacs.d/bin/doom refresh -p $out/doom.d -e $out/emacs.d
          '';
          phases = attrs.phases ++ ["postInstall"];
     });

  tmp = emacsWithDoom doomCorePackages;
  result = emacsWithDoom (epkgs:
     # import ./doom-emacs-pkgs.nix epkgs 
     import "${tmp}/doom-emacs-pkgs.nix" epkgs
  );
in
 result

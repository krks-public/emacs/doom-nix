;; -*- no-byte-compile: t; -*-
;;; g/haskell/packages.el

(package! haskell-mode)

;;
(cond ((featurep! +dante)
       (package! dante)
       (package! attrap)
       (when (featurep! :completion company)
         (package! company-ghc)))
      (t
       (package! intero)
       (package! hindent)))


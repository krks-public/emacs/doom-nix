;; -*- no-byte-compile: t; -*-
;;; g/elm/packages.el

(package! flycheck-elm)
(package! elm-mode)

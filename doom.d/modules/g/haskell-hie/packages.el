;; -*- no-byte-compile: t; -*-
;;; g/haskell/packages.el

(package! haskell-mode)
(package! lsp-haskell)

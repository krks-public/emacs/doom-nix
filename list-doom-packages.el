(require 'cl-lib)

;; (doom-get-packages :backend 'elpa)

;; load doom
(setq emacsd (getenv "EMACSD"))
(load-file (concat emacsd "init.el"))
(require 'core-packages)

(defun pkg-name (pkg)
  "Gets the name of a PKG as string."
  (symbol-name (car pkg)))


(setq pkgs
  (cl-reduce (lambda (acc pkg)
               (concat acc "  " (pkg-name pkg) "\n" ))
             (doom-get-packages
              ;; :backend 'elpa
                                ;; TODO handle quelpa backends too!
                                )
             :initial-value ""))

(princ (concat
        "epkgs:\n"
        "with epkgs;\n"
        "["
        pkgs
        "]"))

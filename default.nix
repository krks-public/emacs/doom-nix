let
  # fetch pinned package set
  nixpkgs = builtins.fetchGit {
       name = "nixpkgs-pinned-2018-12-07";
       url = "https://github.com/NixOS/nixpkgs.git";
       ref = "master";
       rev = "e1b7493cfedba7e715070faa64b77c39d0cf4bff";
  };
  # import the package set we've just fetched
  pinnedPkgs = import nixpkgs {};
  callPackage = pinnedPkgs.callPackage;
in
  callPackage ./doom-emacs.nix { nixpkgs = pinnedPkgs; }
